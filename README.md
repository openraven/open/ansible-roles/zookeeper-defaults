zookeeper-defaults
==================

Ensure the presence of zookeeper paths, and some keys

Requirements
------------

* a working, configured `kubectl` binary on the `$PATH`

Role Variables
--------------


```yaml
zk_paths:
- path: /config/application/thingy
  value: some-value
- path: /config/application/other-thingy
  value: other-value
```

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yaml

    - hosts: kafka-zookeeper-0
      # the target containers do not have python
      gather_facts: no
      connection: kubectl
      # optionally:
      vars:
        ansible_kubectl_namespace: kafka
        ansible_kubectl_container: zookeeper
      roles:
      - role: zookeeper-defaults
```

License
-------

Apache 2

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
